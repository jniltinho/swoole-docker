# Swoole Docker

This should contain all the images that I'll try to build and push to docker.

## Images description

Those images contain PHP 7.x based on alpine with MySQL, PostgreSQL, Redis, GD, [Swoole](https://www.swoole.co.uk/#get-started) enabled and Composer already installed

## Runing the image:

Then run the docker and specify the env file that you have created like this

```
docker run --name swoole-http -p 80:9501 -d jniltinho/swoole-docker
```

## Links

  - https://github.com/linuxjuggler/php-docker-swoole/tree/master/7.3
  - https://github.com/linuxjuggler/php-docker-swoole