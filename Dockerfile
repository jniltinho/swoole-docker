# Builder image NodeJS
FROM jniltinho/alpine-node
FROM php:7.3-alpine3.10
LABEL Maintainer="Nilton OS <jniltinho@gmail.com>"


ENV TZ=America/Sao_Paulo

ENV COMPOSER_ALLOW_SUPERUSER 1

RUN set -ex \
  	&& apk update \
    && apk add --no-cache tzdata libstdc++


RUN set -ex \
    && apk add --no-cache git mysql-client curl openssh-client icu libpng freetype libjpeg-turbo postgresql-dev libffi-dev libsodium libzip-dev libmemcached-libs zlib \
    && apk add --no-cache --virtual build-dependencies icu-dev libxml2-dev zlib-dev libmemcached-dev cyrus-sasl-dev freetype-dev libpng-dev libjpeg-turbo-dev g++ make autoconf libsodium-dev \
    && docker-php-source extract \
    && pecl install swoole redis sodium memcached \
    && docker-php-ext-enable redis swoole sodium memcached \
    && docker-php-source delete \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) pgsql pdo_mysql mysqli mbstring pdo_pgsql intl zip gd \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && cd  / && rm -fr /src \
    && apk del build-dependencies \
    && rm -rf /tmp/* 

RUN addgroup -g 1000 node && adduser -u 1000 -G node -s /bin/sh -D node
#COPY --from=0 /usr/lib/libgcc* /usr/lib/libstdc* /usr/lib/

COPY --from=0 /usr/bin/node /usr/bin/
COPY --from=0 /usr/lib/node_modules /usr/lib/node_modules
COPY --from=0 /usr/include/node /usr/include/node
COPY --from=0 /usr/share/systemtap/tapset/node.stp /usr/share/systemtap/tapset/
COPY --from=0 /usr/local/share/yarn /usr/local/share/yarn


RUN ln -s /usr/local/share/yarn/bin/yarn /usr/local/bin/ \
    && ln -s /usr/local/share/yarn/bin/yarnpkg /usr/local/bin/ \
    && ln -s /usr/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm \
    && ln -s /usr/lib/node_modules/npm/bin/npx-cli.js /usr/local/bin/npx

EXPOSE 9501

COPY index.php /var/www/html/

ENTRYPOINT ["php","/var/www/html/index.php","start"]


    
